package question15;

public class DoubleForLoop {
	
public static void main (String [] args) {
		
		for (int i = 1; i <= 9; i++)
		{
		if (i % 2 != 0) {
		for (int j = i; j <= 9; j++)
		{
		System.out.print(j);
		}

		System.out.println("");
		}
		}
	}

}


/* OUTPUT

123456789
3456789
56789
789
9

*/


