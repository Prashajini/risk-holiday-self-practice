package question16;

public class AnotherDoubleForLoop {
	
	public static void main (String [] args) {
		
		
		int line = 10; 
		
		for(int i = 0; i <= line; i++) {
			for(int j= (line-i-1); j > i; j--) {
				System.out.print(j);
			}
			System.out.println();
		}
		
	}
}


/* OUTPUT

987654321
8765432
76543
654
5

 */





