package question31;

import java.util.Scanner;

public class ToDecimal {
	
	 public static void main(String args[])
     {
         Scanner s=new Scanner(System.in);
         
         System.out.println("Enter a binary number : ");

         String binary=s.next();
         
         System.out.println("Binary number in decimal : " +Integer.parseInt( binary,2));
     }
}


/* OUTPUT

Enter a binary number : 
1111000
Binary number in decimal : 120

*/