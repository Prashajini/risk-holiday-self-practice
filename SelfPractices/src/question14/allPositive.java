package question14;

import java.util.Scanner;

public class allPositive {
	

	public static void main(String[] args) {
		
		double a, b, c;
		
		System.out.print("Enter sideA: ");
		Scanner scan = new Scanner(System.in);
		a = scan.nextDouble();
		System.out.print("Enter sideB: ");
		Scanner scan1= new Scanner(System.in);
		b = scan1.nextDouble();
		System.out.print("Enter sideC: ");
		Scanner scan2= new Scanner(System.in);
		c = scan2.nextDouble();
		
		System.out.print("The values are all positive: " + allPositive(a, b, c));
	}
	
	public static boolean allPositive(double a, double b, double c){
		return ((a>0) && (b>0) && (c>0));

	}
}


/* OUTPUT

Enter sideA: 4
Enter sideB: 5
Enter sideC: 4
The values are all positive: true

*/


