package question24;

public class UnderstandingStringMethods {
	
	public static void main(String[] args) {
		
		String w = "Singing’_in_the_rain";
		String pat = "in";
		

		System.out.println("1) w.indexOf(pat) : "+w.indexOf(pat));
		System.out.println("2) w.indexOf(pat,3) : "+w.indexOf(pat,3));
		System.out.println("3) w.indexOf(pat,6) : "+w.indexOf(pat,6));
		System.out.println("4) w.lastIndexOf(pat) : "+w.lastIndexOf(pat));
		System.out.println("5) w.length() : "+w.length());
		System.out.println("6) w.toUpperCase() : "+w.toUpperCase());
		System.out.println("7) w.charAt(0) : "+w.charAt(0));



	}

}

/* OUTPUT

1) w.indexOf(pat) : 1
2) w.indexOf(pat,3) : 4
3) w.indexOf(pat,6) : 9
4) w.lastIndexOf(pat) : 18
5) w.length() : 20
6) w.toUpperCase() : SINGING’_IN_THE_RAIN
7) w.charAt(0) : S

*/

