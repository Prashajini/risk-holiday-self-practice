package question01;

public class Diamond {
	
public static void main (String [] args) {
		
		int rows = 5;
		
		for (int line = 1; line <= rows; line++) {
			for (int i = 1; i <= rows - line; i++) {
				System.out.print(" ");
			}
			System.out.print("/");

			for (int i = 1; i <= (2 * line) - 2; i++) {
				System.out.print(" ");
			}
			System.out.println("\\");
		}
	
		for (int line = 1; line <= rows; line++) {
			for (int i = 1; i <= line - 1; i++) {
				System.out.print(" ");
			}
			System.out.print("\\");

			for (int i = 1; i <= (rows - line) * 2; i++) {
				System.out.print(" ");
			}
			System.out.println("/");
		}
	}

}


/* EXPECTED OUTPUT
 
    /\
   /  \
  /    \
 /      \
/        \
\        /
 \      /
  \    /
   \  /
    \/
    
*/

/* ACTUAL OUTPUT

    /\
   /  \
  /    \
 /      \
/        \
\        /
 \      /
  \    /
   \  /
    \/

*/


