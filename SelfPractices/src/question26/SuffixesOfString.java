package question26;

public class SuffixesOfString {
	
	public static void main(String[] args) {
		
		String word = "hurricanes";
		int length = word.length();
		
		for (int lines = 0; lines <= length; lines++) {
			
			for (int j = lines; j <= lines; j++) {
				System.out.print(word.substring(j));
			}
			System.out.println();
		}
	}

}

/* OUTPUT

hurricanes
urricanes
rricanes
ricanes
icanes
canes
anes
nes
es
s

*/


