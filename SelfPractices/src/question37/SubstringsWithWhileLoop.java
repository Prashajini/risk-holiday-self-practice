package question37;

public class SubstringsWithWhileLoop {
	
	public static void main(String[] args) {
		
		String word = "sebastian-ibis";
		int word1 = word.length();
		int num = word1 - 1;
		
		
		while (num > 0) {
			System.out.println(word.substring(num));
			num = num - 1;
		}
	}
}


/* OUTPUT
 
s
is
bis
ibis
-ibis
n-ibis
an-ibis
ian-ibis
tian-ibis
stian-ibis
astian-ibis
bastian-ibis
ebastian-ibis
sebastian-ibis

*/