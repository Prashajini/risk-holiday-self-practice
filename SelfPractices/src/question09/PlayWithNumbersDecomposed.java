package question09;

import java.util.Scanner;

public class PlayWithNumbersDecomposed {
	
	public void TwoIntegers(int p, int n) {
		

		Scanner scan = new Scanner(System.in);
		System.out.print("Enter num1 - ");
		p =scan.nextInt();
		
		Scanner scan1 = new Scanner(System.in);
		System.out.print("Enter num2 - ");
		n =scan.nextInt();
		
		int add = n + p;
		int sub = n - p;
		int mul = n * p;
		int div = n / p;
		int mod = n % p;
		
		 System.out.println(" n+p is equal to " + add);
		 System.out.println(" n-p is equal to " + sub);
		 System.out.println(" n*p is equal to " + mul);
		 System.out.println(" n/p is equal to " + div);
		 System.out.println(" n%p is equal to " + mod);
		 System.out.println("****************************");
		
	}

public void ThreeIntegers(int p, int n, int r) {
	
	Scanner scan = new Scanner(System.in);
	System.out.print("Enter num1 - ");
	p =scan.nextInt();
	
	Scanner scan1 = new Scanner(System.in);
	System.out.print("Enter num2 - ");
	n =scan.nextInt();
	
	Scanner scan2 = new Scanner(System.in);
	System.out.print("Enter num1 - ");
	r =scan2.nextInt();
	
	
	int num1 = (p - n)/r;
	int num2 = (p - n)/p;
	int num3 = (n - r)/p;
	int num4 = (n - p)/r;
	int num5 = (r - p)/n;
	int num6 = (r - n)/p;
	
	System.out.println( "(p - n)/r is equal to " +num1);
	System.out.println( "(p - n)/p is equal to " +num2);
	System.out.println( "(n - r)/p is equal to " +num3);
	System.out.println( "(n - p)/r is equal to " +num4);
	System.out.println( "(r - p)/n is equal to " +num5);
	System.out.println( "(r - n)/p is equal to " +num6);
	System.out.println("********************************");
	
	
	
}
}

/* OUTPUT
 
Enter num1 - 20
Enter num2 - 30
n+p is equal to 50
n-p is equal to 10
n*p is equal to 600
n/p is equal to 1
n%p is equal to 10
****************************
Enter num1 - 20
Enter num2 - 12
Enter num1 - -7
(p - n)/r is equal to -1
(p - n)/p is equal to 0
(n - r)/p is equal to 0
(n - p)/r is equal to 1
(r - p)/n is equal to -2
(r - n)/p is equal to 0
*******************************

*/
