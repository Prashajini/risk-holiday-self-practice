package question29;

import java.util.Scanner;

public class NumeralSum {
	
	public static void main(String args[]) {
		
        int sum = 0;
       
        Scanner scan = new Scanner(System.in);
		System.out.print("Input : ");
		String word1=scan.next();
		
		for(int i=0; i< word1.length(); i++) {
		    char  word=word1.charAt(i);
		    if (Character.isDigit(word)) {
		        int b = Integer.parseInt(String.valueOf(word));
		        sum=sum+b;
		    }
		}
		System.out.println("Sum : " +sum);

}
}

/* OUTPUT

Input :  BIO542L
Sum : 11

*/