package question06;

import java.util.Scanner;

public class ComputeTaxAndTotal {
	
	public static void main(String[] args) {

		Scanner scan = new Scanner (System.in);
		System.out.print("Enter the subtotal :");
		int subtotal = scan.nextInt();
		
		
		Scanner scan1 = new Scanner (System.in);
		System.out.print("Enter the tax rate :");
		double taxRate = scan1.nextDouble();
		int tax = (int) ((subtotal * taxRate) /100);
		int total = (int) (subtotal+tax);
		
		System.out.println("----------------------------------------");
		
		int subtotal1 = subtotal/100;
		int subtotal2 = subtotal%100;
		int tax1      = tax/100;
		int tax2      = tax%100;
		int total1    = total/100;
		int total2    = total%100;
		
		System.out.println("The subtotal = " + subtotal1 + " dollars and " + subtotal2 + " cents");
		System.out.println("The tax rate = " + taxRate + " percent.");
		System.out.println("The tax = " + tax1 + " dollars and " + tax2 + " cents");
		System.out.println("The total = " + total1 + " dollars and " + total2 + " cents");
		
		}
	}

	/* OUTPUT
	  
	 
	Enter the subtotal :11050
	Enter the tax rate :5.5
	----------------------------------------
	The subtotal = 110 dollars and 50 cents
	The tax rate = 5.5 percent.
	The tax = 6 dollars and 7 cents
	The total = 116 dollars and 57 cents

	*/


