package question21;

import java.util.Scanner;

public class ReceiveAndPrint {
	
	public static void main (String [] args) {
		
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter String Value : ");
		String s = scan.nextLine();
		
		System.out.print("Enter Integer Value : ");
		int m = scan.nextInt();
		
		System.out.print("Enter Double Value : ");
		double d = scan.nextDouble();
		
		System.out.println(" ");
		System.out.println(s);
		System.out.println(m);
		System.out.println(d);
		System.out.println(m + d + s);
		System.out.println(m + s + d);
		System.out.println(s + m + d);
	}
}
	

/* OUTPUT

Enter String Value : Prasha
Enter Integer Value : 18
Enter Double Value : 5.3

Prasha
18
5.3
23.3Prasha
18Prasha5.3
Prasha185.3

*/


