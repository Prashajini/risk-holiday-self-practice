package question07;

public class SpeedingFine {
	
	public static void main(String[] args) {
		
		int speed, limit, fine;
		speed =50;
		limit=35;
		fine=(speed-limit)*20;
		System.out.println("The fine for driving at " +speed+ " mph on a " +limit+ " mph road is " +fine+ " dollars.");
		speed =30;
		limit=25;
		fine=(speed-limit)*20;
		System.out.println("The fine for driving at " +speed+ " mph on a " +limit+ " mph road is " +fine+ " dollars.");
		speed =60;
		limit=45;
		fine=(speed-limit)*20;
		System.out.println("The fine for driving at " +speed+ " mph on a " +limit+ " mph road is " +fine+ " dollars.");
		}
}


/* OUTPUT
The fine for driving at 50 mph on a 35 mph road is 300 dollars.
The fine for driving at 30 mph on a 25 mph road is 100 dollars.
The fine for driving at 60 mph on a 45 mph road is 300 dollars.
*/



