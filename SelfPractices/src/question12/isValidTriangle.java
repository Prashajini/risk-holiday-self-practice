package question12;

import java.util.Scanner;

public class isValidTriangle {
	
public static void main(String[] args) {
		
		double a, b, c;
		System.out.print("Enter sideA: ");
		Scanner scan = new Scanner(System.in);
		a = scan.nextDouble();
		System.out.print("Enter sideB: ");
		Scanner scan1 = new Scanner(System.in);
		b = scan1.nextDouble();
		System.out.print("Enter sideC: ");
		Scanner scan2 = new Scanner(System.in);
		c = scan2.nextDouble();
	
		System.out.print("Is valid Triangle: " + isValidTriangle(a, b, c));
		
	}
	public static boolean isValidTriangle(double a, double b, double c){
		return ((a+b)>c) && ((a+c)>b) && ((b+c)>a);

	}
}

/* OUTPUT
 
Enter sideA: 5
Enter sideB: 5
Enter sideC: 6
Is valid Triangle: true


 */




