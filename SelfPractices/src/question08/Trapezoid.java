package question08;

public class Trapezoid {
	
	public static void main(String[] args) {
		
		double top, bottom, height, area;
		
		top=10.0;
		bottom=20.5;
		height=24.4; 
		
		area=(bottom+top)*height/2;
		
		System.out.print("Top: ");
		System.out.println(top);
		System.out.print("Bottom: ");
		System.out.println(bottom);
		System.out.print("Height: ");
		System.out.println(height);
		System.out.print("Area: ");
		System.out.println(area);

}
}

/*	OUTPUT

Top: 10.0
Bottom: 20.5
Height: 24.4
Area: 372.09999999999997

*/


