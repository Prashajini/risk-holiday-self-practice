package question13;

import java.util.Scanner;

public class isRightAngleTriangle {
	
public static void main(String[] args) {
		
		double a, b, c;
		
		System.out.print("Enter sideA: ");
		Scanner scan = new Scanner(System.in);
		a = scan.nextDouble();
		System.out.print("Enter sideB: ");
		Scanner scan1= new Scanner(System.in);
		b = scan1.nextDouble();
		System.out.print("Enter sideC: ");
		Scanner scan2= new Scanner(System.in);
		c = scan2.nextDouble();
		
		System.out.print("Is Right Angle Triangle: " + isRightAngleTriangle(a, b, c));
	}
	
	public static boolean isRightAngleTriangle(double a, double b, double c) {
		
		if ((a*a)+(b*b)==(c*c)){
		return true;
		}
		else {
		
		}
		return false;
	}
}

/* OUTPUT

 Enter sideA: 8
Enter sideB: 15
Enter sideC: 17
Is Right Angle Triangle: true

 */


