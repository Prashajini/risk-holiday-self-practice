package question02;

public class DiamondFilled {
	
public static void main (String [] args) {
		
		int rows = 5;
		
		for (int line = 1; line <= rows; line++) {
			for (int i = 1; i <= rows - line; i++) {
				System.out.print(" ");
			}
			for (int k = 1; k <= line; k++) {
				System.out.print("/");
			}
			for (int k = 1; k <= line; k++) {
				System.out.print("\\");
			}
			System.out.println();
		}
		
		for (int line = 1; line <= rows; line++) {
			for (int i = 1; i <= line - 1; i++) {
				System.out.print(" ");
			}
			for (int i = 1; i <= rows - line + 1; i++) {
				System.out.print("\\");
			}
			for (int k = 1; k <= rows - line + 1; k++) {
				System.out.print("/");
			}
			System.out.println();
		}
			
	}
}


/* OUTPUT

	/\
   //\\
  ///\\\
 ////\\\\
/////\\\\\
\\\\\/////
 \\\\////
  \\\///
   \\//
    \/
    
    */


