package question28;

import java.util.Scanner;

public class StringReverse {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter an input String : ");
		String o = scan.nextLine();
		String reverse = ""; 
		
		int length = o.length();
		for (int i = length - 1; i >= 0; i--) {
			reverse = reverse + o.charAt(i);
		}
		System.out.println("The reverse of the input " + o + " is " + reverse + ". ");
	}
}


/* OUTPUT

Enter an input String : Computer-Programming
The reverse of the input Computer-Programming is gnimmargorP-retupmoC. 

*/

